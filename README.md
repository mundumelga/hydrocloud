This project comprises two parts:

1. Hydrofeed 

This component will track the hydroponic sensors feed into RRD tables and push procedures to a cloud location (ssh based). The current implementation uses a RaspberryPi's GPIO inputs with a DS18B20 for water temperature collection and a DHT11 for air temperature and humidity.

2. Hydroweb

This component will be stored in a cloud server. It should be located in the same server where the Hydrofeed pushes data into. So far the mojolicious app is available only to retrieve raw data in json format.