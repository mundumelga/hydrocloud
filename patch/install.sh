#!/bin/bash

wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.36.tar.gz
tar -zxvf bcm*
cd bcm*
./configure
make
make install

wget http://search.cpan.org/CPAN/authors/id/M/MI/MIKEM/Device-BCM2835-1.0.tar.gz
tar -zxvf Device*
cd Device*
perl Makefile.PL
make
make install

apt-get -y install libmojolicious-perl upstart librrdtool-oo-perl

#reboot