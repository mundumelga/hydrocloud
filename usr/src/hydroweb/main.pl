#!/usr/bin/perl

use Mojolicious::Lite;
use lib '/usr/src/hydroweb/lib';
use RRDFeed;

app->config(hypnotoad => {listen => ['http://*:8080']});


do "/usr/src/hydroweb/modules/hum.pl";
do "/usr/src/hydroweb/modules/temp.pl";

app->start;