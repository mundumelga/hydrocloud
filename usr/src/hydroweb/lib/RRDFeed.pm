package RRDFeed;

use strict;
use Exporter;
use RRDTool::OO;
use File::Basename;
 
my $rrd_db = "/usr/share/nginx/www/";
my $params = { 1 => "temp", 2 => "hum" };

my $rrd = {};

sub get_feed
{
	my ($dsname) = @_;
	
	if ($rrd->{$dsname})
	{
		return $rrd->{$dsname};
	}
	else
	{
		$rrd->{$dsname} = RRDTool::OO->new( file => $rrd_db.$dsname.".rrd" );
		return $rrd->{$dsname};
	}
}

sub get_ds_values
{
	my ($dsname, $start_time, $end_time) = @_;
	
	my $rrd = get_feed($dsname);
	
	print "Going for it\n";
	
	if ($end_time && $start_time)
	{
		$rrd->fetch_start(start => $start_time,
                      end   => $end_time);
        print "Start time: $start_time with end_time : $end_time\n";
	}
	elsif($start_time)
	{
		$rrd->fetch_start(start => $start_time,
                      end   => time());
        print "Start time: $start_time with end_time : $end_time\n";
	}
	else #Very dangerous
	{
		$rrd->fetch_start();
		print "Start time: $start_time with end_time : $end_time\n";
	}
 
 	my $results;                     
    # Skip undefineds
    #$rrd->fetch_skip_undef();

    # Fetch stored values
    while(my($time, $value) = $rrd->fetch_next()) {
         $results->{$time} = $value;
    }
    
    return $results;               
 
}



1;