#!/usr/bin/perl
#use Mojolicious::Lite;

get '/temp' => sub {
	my $self = shift;

	# TODO: Validate dates
	my $data = RRDFeed::get_ds_values("temp");
	
	return $self->render( json => $data);
};

get '/temp/:startdate/:enddate' => sub {
	my $self = shift;
	
	my $enddate = $self->param("enddate");
	my $startdate = $self->param("startdate");
	
	# TODO: Validate dates
	my $data = RRDFeed::get_ds_values("temp", $startdate, $enddate);
	
	return $self->render( json => $data);
};
