#!/usr/bin/perl
#use Mojolicious::Lite;

get '/hum' => sub {
	my $self = shift;
	
	# TODO: Validate dates
	my $data = RRDFeed::get_ds_values("hum");
	
	return $self->render( json => $data);
};

get '/hum/:startdate/:enddate' => sub {
	my $self = shift;
	
	my $enddate = $self->param("enddate");
	my $startdate = $self->param("startdate");
	
	# TODO: Validate dates
	my $data = RRDFeed::get_ds_values("hum", $startdate, $enddate);
	
	return $self->render( json => $data);
};
