#!/usr/bin/perl
use RRDTool::OO;
use lib '/usr/src/hydrofeed/lib/';
use Log;
use RRDFeed;

my $temp_db = "/usr/share/nginx/www/temp.rrd";
my $hum_db  = "/usr/share/nginx/www/hum.rrd";

use strict;

my $temph = RRDFeed::get_handler($temp_db);
my $humh = RRDFeed::get_handler($hum_db);

my $script    = "/usr/src/hydrofeed/dht11/dht11";
my $dht_model = 11;
my $gpio_port = 17;

my $output = "";

my $count = 0;
my $max_count = 5;

while  ( $output !~ /Temp/) {
	
	if ($count == $max_count)
	{
		Log::this ("I'm out. No valid values found");
		exit 1;
	}
	
	$output = `$script $dht_model $gpio_port`;
	$count++;
	Log::this ("Going for tryout number $count");
	sleep 5;
}



my ($temp) = $output =~ /Temp = (\d+).*,/;
my ($hum)  = $output =~ /Hum = (\d+)/;

my $epoc = time();

print "Temperature is $temp C\n";
print "Humidity is $hum %\n";

Log::this( "Temperature: $temp, Humidity: $hum");

my $current_time = time();

if ($temp == 255)
{
	Log::this ("We should skip this: 255");
	exit 1;
}

print "Storing temperature values in DB\n";
$temph->update(
	time   => $current_time,
	values => {
		"temp" => $temp
	}
);

print "Storing humidity values in DB\n";
$humh->update(
	time   => $current_time,
	values => {
		"hum" => $hum
	}
);