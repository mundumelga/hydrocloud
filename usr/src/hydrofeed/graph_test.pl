#!/usr/bin/perl

use RRDTool::OO;
use Net::SCP::Expect;

sleep 30;

my $params = { 1 => "temp", 2 => "hum", 3 => "watert" };

my $dir     = "/usr/share/nginx/www/";
my $hourly  = "hourly";
my $daily   = "daily";
my $weekly  = "weekly";
my $monthly = "monthly";

my $route_hash = {
	$hourly  => 3600,
	$daily   => 24 * 3600,
	$weekly  => 7 * 24 * 3600,
	$monthly => 30 * 24 * 3600

};

sub graph_hum {

	my ( $dsname, $routine ) = @_;

	my $rrd = RRDTool::OO->new( file => $dir.$dsname.".rrd" );

	# Draw a graph in a PNG image
	$rrd->graph(
		image          => $dir.$dsname."_".$routine.".png",
		vertical_label => 'Sensoring',
		start          => time() - $route_hash->{$routine},
		#upper_limit    = 100,
		draw           => {
			type   => "area",
			dsname => $dsname,
			color  => 'FF00FF',
			legend => $dsname,
		}
	);
}

while ( ( $key, $param ) = each $params ) {

	# This is safe
	while ( ( $routine, $timestamp ) = each $route_hash ) {
		graph_hum($param, $routine);
	}
}



`logger -t "DHT Grapher" "Created graphs."`;

