#!/usr/bin/perl

use lib '/usr/src/hydrofeed/lib/';
use Log;
use RRDFeed;
use strict;

my $water_temp = "/usr/share/nginx/www/watert.rrd";
my $is_celsius = 1; #set to 1 if using Celsius

my $temp_h = RRDFeed::get_handler($water_temp);

my $epoc = time();

Log::this("Water temperature");

my $current_time = time();

my $modules = `cat /proc/modules`;
if ($modules =~ /w1_therm/ && $modules =~ /w1_gpio/)
{
        #modules installed
}
else
{
       my $gpio = `sudo modprobe w1-gpio`;
        my $therm = `sudo modprobe w1-therm`;
}
 
my $output = "";
my $attempts = 0;
my $temp = "";

while ($output !~ /YES/g && $attempts < 5)
{
        $output = `sudo cat /sys/bus/w1/devices/28-*/w1_slave 2>&1`;
        if($output =~ /No such file or directory/)
        {
                Log::this "Could not find DS18B20\n";
                last;
        }
        elsif($output !~ /NO/g)
        {
                $output =~ /t=(\d+)/i;
                $temp = ($is_celsius) ? ($1 / 1000) : ($1 / 1000) * 9/5 + 32;
                
                Log::this ( "Current water temperature ".$temp);
        }
 
        $attempts++;
}

print "Storing temperature values in DB\n";
$temp_h->update(
	time   => $current_time,
	values => { "watert" => $temp }
);
