package RRDPush;
use Net::SCP::Expect;
use lib '/usr/src/hydrofeed/lib/';
use Log;

my $params = { 1 => "temp", 2 => "hum", 3 => "watert" };
my $dir = "/usr/share/nginx/www/";

sub convert_to_xml {
	while ( ( $key, $param ) = each $params ) {

		my $origin = $dir . $param . ".rrd";
		my $dst = $dir . $param . ".rrd.xml";
		`rrdtool dump $origin > $dst`;
	}
}

sub send_rrd {

	my $scpe = Net::SCP::Expect->new(
		host          => '54.72.159.214',
		user          => 'ubuntu',
		identity_file => '/home/pi/hydrofeed.pem'
	);

	convert_to_xml();
	
	while ( ( $key, $param ) = each $params ) {

		Log::this("Sending $param database");
		$scpe->scp( $dir . $param . ".rrd.xml", $dir );
	}

	#$scpe->close();
}

1;
