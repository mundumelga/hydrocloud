#
# Round Robin Database Feed Module
#
use RRDTool::OO;
use Log;
use lib '/usr/src/hydrofeed/common/';
use File::Basename;

package RRDFeed;

sub get_handler {
	
	my ($db_file) = @_;
	
	my ($ds_name, $directory, $extension) = File::Basename::fileparse($db_file, qr/\.[^.]*/);
	
	print $ds_name,"\n";
	
	my $rrd;
	
	if ( !-f $db_file ) {
		print "Database does not exist\n";
		Log::this("Database $db_file does not exist");

		# Constructor
		$rrd = RRDTool::OO->new( file => $db_file );

		# Create a round-robin database
		$rrd->create(
			step        => 60,    # one-second intervals
			data_source => {
				name => $ds_name,
				type => "GAUGE",

			},
			archive => { rows => 21600, }
		);
	}
	else {
		$rrd = RRDTool::OO->new( file => $db_file );
	}
	
	return $rrd;
}

1;